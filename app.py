from flask import Flask, request, render_template
import sqlite3
from flask import g
import werkzeug

import inp

werkzeug.cached_property = werkzeug.utils.cached_property
from tensorflow.keras.models import load_model
from inp import pred_value

new_model = load_model("model_prediction/MGMT_prediction.h5")

app = Flask(__name__)
app.config['UPLOAD_PATH'] = "C://Users/kavya nayak/Downloads/rsna-miccai-brain-tumor-radiogenomic-classification/test"



DATABASE = './users.db'


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv


def valid_login(username, password):
    user = query_db('select * from User where username = ? and password = ?', [username, password], one=True)
    if user is None:
        return False
    else:
        return True


def log_the_user_in(username):
    return render_template('forest.html', username=username)


@app.route("/")
@app.route('/login', methods=['POST', 'GET'])
def login():
    error = None
    if request.method == 'POST':
        if valid_login(request.form['username'], request.form['password']):
            return log_the_user_in(request.form['username'])
        else:
            error = 'Invalid username/password'

    return render_template('login.html', error=error)


def predict(a, types):
    value = inp.pred_value(a, types)
    print(value)
    return value

@app.route("/uplo", methods=['POST'])
def uplo():
     num = request.form.get('number')
     a=int(num)
     print(a)
     types = request.form.get("data")
     print(types)
     output=predict(a,types)
     return render_template("predict.html",prediction=output)






    # print(output.MGMT_VALUE)

''' if output > float(0.5):
         return render_template('predict.html',prediction='Your Forest is in Danger.\nProbability of fire occuring is {}'.format(output))
     else:
         return render_template('predict.html', prediction='Your Forest is safe.\n Probability of fire occuring is {}'.format(output))
'''


if __name__ == "__main__":
    app.run(debug=True)
