import cv2 as cv
from keras.models import load_model
import os
import numpy as np
import pandas as pd

import pydicom

new_model = load_model("model_prediction/MGMT_prediction.h5")

train_df = pd.read_csv('C://Users/kavya nayak/Downloads/rsna-miccai-brain-tumor-radiogenomic-classification/train_labels.csv')
sample_df = pd.read_csv('C://Users/kavya nayak/Downloads/rsna-miccai-brain-tumor-radiogenomic-classification/sample_submission.csv')
test_dir = 'C://Users/kavya nayak/Downloads/rsna-miccai-brain-tumor-radiogenomic-classification/test'
testset = []
testidt = []

def load_dicom(path):
    dicom=pydicom.read_file(path)
    data=dicom.pixel_array
    data=data-np.min(data)
    if np.max(data) != 0:
        data=data/np.max(data)
    data=(data*255).astype(np.uint8)
    return data


def pred_value(num, types):
  testset = []
  testidt = []

  idt = sample_df.loc[num, 'BraTS21ID']
  idt2 = ('00000' + str(idt))[-5:]
  path = os.path.join(test_dir, idt2, types)
  for im in os.listdir(path):
      img = load_dicom(os.path.join(path, im))
      img = cv.resize(img, (64, 64))
      image = np.array(img)
      image = image / 255.0
      testset += [image]
      testidt += [idt]
  X_test = np.array(testset)

  new_model = load_model("model_prediction/MGMT_prediction.h5")
  model=new_model
  y_pred = model.predict(X_test)
  pred=np.argmax(y_pred, axis=1)
  result=pd.DataFrame(testidt)

  result[1]= pred
  result.columns=['BraTS21ID','MGMT_value']
  #result2 = result._get_value("'BraTS21ID","MGMT_value")
  result2 = result.groupby('BraTS21ID', as_index=False).mean()
  return result2

#print(pred_value(1,"FLAIR"))


